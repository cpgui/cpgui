# Changelog

## 1.0

### Beta 1.0.1

* Change/Remove documentation. Now you must create your own documentation!
* Add build.sh and build.bat
* Fix rainbow error and removed rainbow finally
* Fix language

### Beta 1.0

* Update docs with more documentations for the methods

## 0.2

### Alpha 0.2.4

* Update docs

### Alpha 0.2.3

* Update docs
* Removed `rainbow` from dependencies

### Alpha 0.2.2

* Update docs

#### Core

* Delete `cpgui.json` and add the properties in `config.json`
* Fix `CPGUI#version`

#### Main module

* Add module reload with `reload [<module>]`

### Alpha 0.2.1

* Change language to english

### Alpha 0.2

#### Core

* Add module structure (new `module.json`)
  * name
  * version
  * website
  * devs
* New class `CPGUI::AppClass`
* Move commands and enable state in `CPGUI::AppClass`
* Update language
* Bug fixes

#### Main module

* Add new commands
  * `status` (Get information of the core system)
  * `status <module>` (Get information of the module)
* Remove commands
  * `main`
  * `cpgui`

## 0.1

### Alpha 0.1.2

* Fix help bug
* Add changelog

### Alpha 0.1.1

Prevent override send method from the kernel

* Update README
* Rename `send` to `send_message`

### Alpha 0.1

First release! Test and give feedback :)
