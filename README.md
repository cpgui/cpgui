# cpgui

![Logo](https://gitlab.com/cpgui/cpgui/raw/master/assets/logo.png "Logo")

[![Build Status](https://gitlab.com/cpgui/cpgui/badges/master/pipeline.svg)](https://gitlab.com/cpgui/cpgui/commits/master)
[![Join discord](https://img.shields.io/discord/586558998749118467)](https://discord.gg/Z6ZsxaG)

A cross-plattforming app!

## Currently in break

Please contact me, if you create a module to increase my motivation! Thanks :)

## Installation

1. Install git: <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>
2. Install [rbenv](https://github.com/rbenv/rbenv) and install ruby
3. Download the repository with the command: `git clone https://github.com/cpgui/cpgui.git` to get the latest build or download the latest release here <https://gitlab.com/cpgui/cpgui/releases>
4. Go to this directory with `cd cpgui`
5. Run this app with `ruby index.rb`

## Features

* for all plattforms
* multi-language support
* multi program language support _(in development)_
* unlimited modules
* customizable modules
* you can create your own modules!
* you can create a module for multiple plattforms _(for example discord and slack)_

## Building the documentation

1. Install yard: `gem install yard`
2. Build the documentation:
    **Linux:**
        `sh start.sh`
    **Windows:**
        `./start.bat`

## Screenshot

![Console screenshot](https://gitlab.com/cpgui/cpgui/raw/master/assets/screenshot.png "Screenshot from the application")

## Guide

[Visit the wiki](https://gitlab.com/cpgui/cpgui/wikis) to see the guide! Click [here](https://cpgui.gitlab.io/cpgui/index.html) to open the documentation.

## Discord

<https://discord.gg/Z6ZsxaG>
