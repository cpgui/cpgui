# frozen_string_literal: true

# @return [void]
def send_message(message)
  puts "\e[35m[Loader] " + message + "\e[0m"
end
send_message "\e[33mWelcome to the CPGUI! Starting app..."
send_message "\e[33mReading lib folder..."
Dir[File.join('./lib', '**/*.rb')].each do |file|
  send_message "\e[33mIncluding #{file}..."
  require_relative file
  send_message "\e[32mSuccessfully included #{file}!"
end
send_message "\e[32mSuccessfully read lib folder!"
app = CPGUI.new
send_message "\e[32mWelcome to the CPGUI! Successfully started app!"
app.run
