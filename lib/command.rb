# frozen_string_literal: true

class CPGUI
  # A console command
  class Command
    # @param aliases [Array(String)]
    # @yield [command, args]
    def initialize(aliases, &block)
      @aliases = aliases
      @method = block
    end

    # @return [Array(String)]
    attr_accessor :aliases
    # @return [Proc]
    attr_accessor :method

    # @return [void]
    def call(command, args)
      method.call(command, args)
    end
  end
end
