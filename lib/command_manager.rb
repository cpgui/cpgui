# frozen_string_literal: true

class CPGUI
  # Command manager for the module manager
  class CommandManager
    # @param module_manager [CPGUI::ModuleManager]
    def initialize(module_manager)
      @config = CPGUI::Configuration.new(File.join(__dir__, '../config.json'))
      language_dir = File.join(__dir__, '../language')
      @language = CPGUI::Language.new(@config.get('language'), language_dir)
      @module_manager = module_manager
    end

    # Handle command on every module
    # @param command_string [String]
    # @param args [Array(String)]
    # @return [Object,nil]
    def handle_command(command_string, args)
      modules_commands = {}
      @module_manager.modules.each do |app_class|
        commands = app_class.commands.select do |_symbol, command|
          return false if command.aliases.nil?

          true if command.aliases.include? command_string
        end
        permit = app_class.enabled? && !commands.empty?
        modules_commands[app_class] = commands.keys if permit
      end
      handle_module_commands(modules_commands, command_string, args)
    end

    # Handle command from this array
    # @param modules_commands [Hash(CPGUI::AppModule, Array(String))]
    #   used by CommandManager#handle_command
    # @param command_string [String]
    # @param args [Array(String)]
    # @return [Object,nil]
    def handle_module_commands(modules_commands, command_string, args)
      return nil if modules_commands.empty?

      unless modules_commands.length == 1
        current_module = input_module(modules_commands.keys)
      end
      current_module = modules_commands.keys.first if current_module.nil?
      symbols = modules_commands[current_module]
      handle_symbol_commands(symbols, current_module, command_string, args)
    end

    # Handle command from this array
    # @param symbols [Array(Symbol)]
    #   used by CommandManager#handle_module_commands
    # @param app_class [CPGUI::AppModule]
    # @param command_string [String]
    # @param args [Array(String)]
    # @return [Object,nil]
    def handle_symbol_commands(symbols, app_class, command_string, args)
      return nil if symbols.empty?

      current_symbol = input_command(symbols) unless symbols.length == 1
      current_symbol = symbols.first if current_symbol.nil?
      return unless app_class.commands.keys.include? current_symbol

      app_class.commands[current_symbol].call(command_string, args)
    end

    # @param modules [Array(CPGUI::AppClass)]
    # @return [CPGUI::AppClass] current selected module
    def input_module(modules)
      delimiter = @language.get('command', 'multiple', 'modules', 'delimiter')
      module_string_list = @module_manager.module_strings
      module_string = module_string_list.join(delimiter)
      output = @language.get('command', 'multiple', 'modules', 'output')
      input = ''
      until module_string_list.include? input
        send_message format(output, modules: module_string)
        input = gets.chomp
      end
      modules[module_string_list.index(input)]
    end

    # @param commands [Array(Symbol)]
    # @return [Symbol] current symbol
    def input_command(commands)
      delimiter = @language.get('command', 'multiple', 'commands', 'delimiter')
      command_string = commands.join(delimiter)
      command_string_list = commands.map(&:to_s)
      output = @language.get('command', 'multiple', 'commands', 'output')
      input = ''
      until command_string_list.include? input
        send_message format(output, commands: command_string)
        input = gets.chomp
      end
      commands[command_string_list.index(input)]
    end

    # @return [void]
    def send_message(message)
      puts prefix + message + "\e[0m"
    end

    # @return [String]
    def prefix
      @language.get('command', 'prefix')
    end
  end
end
