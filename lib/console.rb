# frozen_string_literal: true

require 'socket'
class CPGUI
  # Handle the console commands
  # @note #WIP
  class ConsoleManager
    # @param cpgui [CPGUI] the app instance
    def initialize(cpgui)
      @config = CPGUI::Configuration.new(File.join(__dir__, '../config.json'))
      language_dir = File.join(__dir__, '../language')
      @language = CPGUI::Language.new(@config.get('language'), language_dir)
      @cpgui = cpgui
    end

    # Run the console manager
    # @return [void]
    def run
      send_message @language.get('console', 'run')
      @run = true
      input while @run
    end

    # Stop the console manager
    # @return [void]
    def stop
      send_message @language.get('console', 'stop')
      @run = false
    end

    # Test if console manager is running
    # @return [Boolean]
    def run?
      @run
    end

    # The prefix of the console manager
    # @return [String]
    def prefix
      @language.get('console', 'prefix')
    end

    # Send the message with the prefix
    # @param message [String]
    # @return [void]
    def send_message(message)
      puts prefix + message
    end

    # @return [CPGUI]
    attr_reader :cpgui

    private

    # Print prefix and handle the input of the console
    # @return [void]
    def input
      print_prefix
      handle_input
    rescue StandardError, Interrupt => e
      print_error(e) if e.is_a? StandardError
      puts "\r\n"
      send_message @language.get('console', 'exit')
      @cpgui.stop
      exit
    end

    # @param error [StandardError]
    # @return [void]
    def print_error(error)
      send_message error.message + "\r\n"
    end

    # Handle input
    # @return [void]
    def handle_input
      command = split_input(gets)
      return false if command.nil?

      console_manager = @cpgui.module_manager.command_manager
      exist = console_manager.handle_command(command[:prefix], command[:args])
      send_message @language.get('console', 'notfound') unless exist
    end

    # Print the prefix in the console
    # @return [void]
    def print_prefix
      out = @language.get('console', 'input')
      version = @cpgui.version
      user = ENV['USERNAME']
      print format(out, version: version, user: user)
    end

    # Split the input in command and arguments
    # @return [Hash] :prefix and :args
    def split_input(input)
      input = input.chomp
      input_list = input.split(' ')
      return if input_list.empty?

      command_prefix = input_list[0]
      command_args = input_list[1..-1]
      { prefix: command_prefix, args: command_args }
    end
  end
end
