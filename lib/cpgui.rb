# frozen_string_literal: true

require 'json'
# The main class of the app
class CPGUI
  def initialize
    @console_manager = CPGUI::ConsoleManager.new(self)
    @module_manager = CPGUI::ModuleManager.new(self)
    @config = CPGUI::Configuration.new(File.join(__dir__, '../config.json'))
    @version = @config.get('version')
    language_dir = File.expand_path('../language', __dir__)
    @language = CPGUI::Language.new(@config.get('language'), language_dir)
    @module_manager.detect
  end

  # Run the cpgui
  # @return [void]
  def run
    @config.reload
    @language.language = @config.get('language')
    @language.reload
    @module_manager.start
    @console_manager.run
  end

  # Stop the cpgui
  # @return [void]
  def stop
    @module_manager.stop
    @console_manager.stop
  end

  # The version of the cpgui
  # @return [String]
  attr_reader :version

  # @return [CPGUI::ConsoleManager]
  attr_reader :console_manager
  # @return [CPGUI::ModuleManager]
  attr_reader :module_manager
end
