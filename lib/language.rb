# frozen_string_literal: true

require 'json'

class CPGUI
  # Language system for the cpgui
  class Language
    def initialize(language, folder)
      @folder = folder
      @languages = {}
      reload
      @language = language if @languages.key? language
    end

    # Get the language of the translation
    # @return [String]
    attr_reader :language

    # Set the language of the translation
    # @return [void]
    def language=(value)
      @language = value if @languages.key? value
    end

    # Returns a hash with the file name as key
    # @return [Hash<String,Hash>]
    attr_reader :languages

    # The folder of the translations
    # @return [String]
    attr_reader :folder

    # Get a value in the language file
    # @return [Object, nil]
    def get(*keys)
      current_section = @languages[@language]
      return nil if current_section.nil?

      keys.each do |key|
        return nil unless current_section.key? key

        current_section = current_section[key]
      end
      current_section
    end

    # Reload current configuration
    def reload
      @languages = {}
      Dir[File.join(@folder, '*.*')].each do |file_string|
        file = File.open(file_string)
        data = JSON.parse file.read
        file.close
        @languages[File.basename(file)] = data
      end
    end
  end
end
