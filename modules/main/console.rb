# frozen_string_literal: true

require 'json'
# Main module for the cpgui
class MainModule
  def command_exit
    aliases = @language.get('commands', 'exit', 'aliases')
    @app_class.register(:exit, aliases) do |_command, _args|
      send_message @language.get('commands', 'exit', 'output')
      @module_manager.cpgui.stop
      puts "\r\n\r\n"
      exit
    end
  end

  def command_reload
    aliases = @language.get('commands', 'reload', 'aliases')
    @app_class.register(:reload, aliases) do |_command, args|
      command_reload_system if args.empty?
      command_reload_module args[0] if args.length == 1
      usage = @language.get('commands', 'reload', 'usage')
      send_message usage if args.length > 1
      true
    end
  end

  def command_reload_module(module_string)
    module_class = @module_manager.get_module_by_string module_string
    unless module_class
      send_message @language.get('commands', 'reload', 'module', 'invalid')
      return
    end
    reloading = @language.get('commands', 'reload', 'module', 'reloading')
    send_message format(reloading, module: module_string)
    module_class.reload
    reloaded = @language.get('commands', 'reload', 'module', 'reloaded')
    send_message format(reloaded, module: module_string)
  end

  def command_reload_system
    send_message @language.get('commands', 'reload', 'system', 'reloading')
    @module_manager.restart
    send_message @language.get('commands', 'reload', 'system', 'reloaded')
  end

  private

  def register_commands
    command_exit
    command_reload
    register_module_commands
    register_info_commands
  end
end
