# frozen_string_literal: true

# Main module for the cpgui
class MainModule
  def command_modules
    aliases = @language.get('commands', 'modules', 'aliases')
    @app_class.register(:modules, aliases) do |_command, args|
      command_modules_list if args.length.zero?
      command_modules_help(args) if args.length == 1
      usage = @language.get('commands', 'modules', 'usage')
      send_message usage if args.length > 1
      true
    end
  end

  def command_status
    aliases = @language.get('commands', 'info', 'aliases')
    @app_class.register(:status, aliases) do |_command, args|
      send_message system_status_message if args.empty?
      command_status_module(args) if args.length == 1
      send_message @language.get('commands', 'info', 'usage') if args.length > 1
      true
    end
  end

  def command_status_module(args)
    return false if args.length != 1

    current_app_class = @module_manager.get_module_by_string args[0]
    if current_app_class.nil?
      send_message @language.get('commands', 'info', 'notexist')
    else
      send_message module_status_message(current_app_class)
    end
  end

  # @return [String]
  def system_status_message
    result = @language.get('commands', 'info', 'output', 'system')
    result = format(result, version: @module_manager.cpgui.version)
    result
  end

  # @param app_class [CPGUI::AppClass]
  def module_status_message(app_class)
    result = @language.get('commands', 'info', 'output', 'module')
    prop = symbolize_keys(app_class.properties.clone)
    prop[:status] = @language.get('commands', 'info', 'status',
                                  if app_class.enabled?
                                    'enabled'
                                  else
                                    'disabled'
                                  end)
    format(result, prop)
  end

  # @param hash [Hash(String, Object)]
  # @return [Hash(Symbol, Object)]
  def symbolize_keys(hash)
    hash.each_with_object({}) { |(k, v), memo| memo[k.to_sym] = v; }
  end

  def command_modules_list
    module_classes = modules
    delimiter = @language.get('commands', 'modules', 'delimiter')
    module_string = module_classes.join(delimiter)
    output = @language.get('commands', 'modules', 'output')
    count = module_classes.length
    send_message format(output, count: count, modules: module_string)
    true
  end

  def command_modules_help(args)
    app_module_class = @module_manager.get_module_by_string(args[0])
    return true if app_module_class.nil?

    send_message @module_manager.help(app_module_class, args[1..-1]).to_s
    true
  end

  def register_info_commands
    command_status
    command_modules
  end
end
