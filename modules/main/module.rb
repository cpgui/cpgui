# frozen_string_literal: true

# Main module for the cpgui
class MainModule
  def command_enable
    aliases = @language.get('commands', 'enable', 'aliases')
    @app_class.register(:enable, aliases) do |_command, args|
      module_class = can_command_enable(args)
      next true unless module_class

      module_class.enable!
      true
    end
  end

  def can_command_enable(args)
    if args.length != 1
      send_message @language.get('commands', 'enable', 'usage')
    else
      module_class = @module_manager.get_module_by_string args[0]
      return module_class unless module_class
    end
  end

  def can_command_disable(args)
    confirm = @language.get('commands', 'disable', 'confirm')
    is_confirm = args.length == 2 && confirm.include?(args[1])
    if args.length == 1 || is_confirm
      module_class = @module_manager.get_module_by_string args[0]
      return nil unless module_class
      return nil unless command_disable_self(module_class, is_confirm)
    else
      send_message @language.get('commands', 'disable', 'usage')
    end
    module_class
  end

  # @param module_class [CPGUI::AppClass]
  # @confirm [Boolean]
  def command_disable_self(module_class, confirm)
    if module_class.app_module.class == self.class && !confirm
      send_message @language.get('commands', 'disable', 'noconfirm')
      false
    else
      true
    end
  end

  def command_disable
    aliases = @language.get('commands', 'disable', 'aliases')
    @app_class.register(:disable, aliases) do |_command, args|
      module_class = can_command_disable args
      next true unless module_class

      module_class.disable!
      true
    end
  end

  def modules
    module_classes = []
    @module_manager.modules.each do |app_class|
      app_module = app_class.app_module
      module_classes.push(app_module.class.name)
    end
    module_classes
  end

  def register_module_commands
    command_disable
    command_enable
  end
end
